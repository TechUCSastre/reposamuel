package com.bbva.qwai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.RequestHeaderParamsName;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;

/**
 * GET /customers/id
 * Business Logic implementation.
 * @author carlos
 *
 */
public class QWAIT00301MXTransaction extends AbstractQWAIT00301MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00301MXTransaction.class);
	
	@Override
	public void execute() {
		QWAIR001 qwaiR001 = (QWAIR001)getServiceLibrary(QWAIR001.class);
		// 
		if(QWAIT00301MXAction.GET.name().equals(getRestfulMethod())){
			LOGGER.info("Estamos entrando al metodo GET del API Customers por Id");
			
			String countryCode = (String) this.getRequestHeader().getHeaderParameter(RequestHeaderParamsName.COUNTRYCODE);
			LOGGER.info("El country code es: " + countryCode);
			
			String customerId = getPathParameter("customerId");
			
			CustomerDTO retorno = qwaiR001.executeGet(customerId);
			
			if (this.getAdvice() != null) {
				if(this.getAdvice().getCode().equals("QWAI00841003")){
					LOGGER.warn("Mas de un resultado");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
				}
				else{
					//QWAI00841003
					LOGGER.warn("No hubo registros");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_204, Severity.WARN);
				}
				
			} else {
				LOGGER.info("Busqueda exitosa");
				setEntity(retorno);
				setContentLocation(getURIPath());
				setHttpResponseCode(HttpResponseCode.HTTP_CODE_200, Severity.OK);
			}
			
			LOGGER.info("Hola somos el Grupo 5 APXXXX");
			
		}
	}

}
