# Training APX

## Portal de documentación

https://globaldevtools.bbva.com/platform_docs/

# Configuración IDE (settings.xml)

#### Actualizar settings.xml

Para realizar este paso necesitamos el API Key de Artifactory y el nombre de usuario.

#### Conseguir API Key Artifactory:

https://globaldevtools.bbva.com/beapp/static/#!/home

Si no se dispone de API Key en Artifactory, pulsar en Generate.

![IMG](images/generate_apikey.png)

#### Modificar el fichero settings.xml (antes de lanzar la instalación del IDE)

* Sustituir el fichero settings.xml de la ruta donde se ha descomprimido el IDE:

```
<path_to_IDE>/tools/apache-maven-3.2.1/conf/settings.xml
```

Una vez dentro del IDE, vamos a crear una unidad funcional (UF)

1. Nuevo > APX > Unidad funcional

![IMG](images/001-QWAI.png)

![IMG](images/qwai_ide.png)


2. Para enlazar los repositorios desde el IDE, haremos lo siguiente:

*Window > Show View > Other > Git > Git Repositories*

Nos muestra en la parte inferior el siguiente panel:

![IMG](images/RepoAPX.png)

3. Para configurar el repo de git al que subiremos nuestra UF haremos un Add an existing local Git repository

*Nota: indicaremos el repositorio de cada grupo, no el del trainer.

![IMG](images/GIT.png)

#falta como subir la UF al repositorio y como desplegar en el PAAS