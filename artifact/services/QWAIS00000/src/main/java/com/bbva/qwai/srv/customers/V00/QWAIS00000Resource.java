package com.bbva.qwai.srv.customers.V00;

import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.elara.physical.restful.annotation.ApxTransactionInfo;
import com.bbva.elara.physical.restful.annotation.ApxUriInfo;
import com.bbva.elara.physical.restful.annotation.PATCH;

@Path("/customers")
@ApxUriInfo(uri = "/customers/v0")
public class QWAIS00000Resource extends AbstractResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIS00000Resource.class);
    @GET
    @Produces(value = { "application/json" })
    @Consumes(value = { "application/json" })
    @Path("/{customerId}")
    @ApxTransactionInfo(transactionCode = "QWAIT003", versionCode = "01", countryCode = "MX", method = "GET", responses = { HttpResponseCode.HTTP_CODE_200, HttpResponseCode.HTTP_CODE_204 })
    public Response GET(@PathParam("customerId") String customerId, @Context MessageContext mc) {
    	LOGGER.info("Entramos en el servicio - piruli");
    	return this.otmaPGApxRestfulService.otmaPgApxRestfulService("QWAIT003", "01", "MX", "GET", mc, this.bundleContext);
    }

    @POST
    @Produces(value = { "application/json" })
    @Consumes(value = { "application/json" })
    @Path("/")
    @ApxTransactionInfo(transactionCode = "QWAIT002", versionCode = "01", countryCode = "MX", method = "POST", responses = { HttpResponseCode.HTTP_CODE_201 })
    public Response POST(Map<String, Object> cuerpo, @Context MessageContext mc) {
        return this.otmaPGApxRestfulService.otmaPgApxRestfulService("QWAIT002", "01", "MX", "POST", cuerpo, mc, this.bundleContext);
    }

    @GET
    @Produces(value = { "application/json" })
    @Consumes(value = { "application/json" })
    @Path("/")
    @ApxTransactionInfo(transactionCode = "QWAIT001", versionCode = "01", countryCode = "MX", method = "GET", responses = { HttpResponseCode.HTTP_CODE_200, HttpResponseCode.HTTP_CODE_204 })
    public Response GET(@QueryParam("paginationKey") String paginationKey, @QueryParam("pageSize") String pageSize, @QueryParam("fields") String fields, @QueryParam("identityDocumentType") String identityDocumentType, @Context MessageContext mc) {
        return this.otmaPGApxRestfulService.otmaPgApxRestfulService("QWAIT001", "01", "MX", "GET", mc, this.bundleContext);
    }
}
