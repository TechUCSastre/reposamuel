behavior(){
  def file = new File(".gitignore")
  if(file.exists()){
    def text = file.text
    def found = ["*.class33",".project","target"].find({ext ->
      !text.contains(ext)}
    )
    return found ? incorrect("Extension $found not exists"): ok()
  }
  return notRequired("Gitignore not found")
}
