package com.bbva.qwai.lib.r002;

public interface QWAIR002 {

	String executeDtoToBdDocType(String documentType);
	String executeBdToDtoDocType(String codTipident);
	
	String executeDtoToBdGender(String gender);
	String executeBdToDtoGender(String xtiSexo);
	
	String executeDtoToBdPersTitle(String personalTittle);
	String executeBdToDtoPersTitle(String codTratanor);
	
	String executeDtoToBdMaritalSt(String maritalStatus);
	String executeBdToDtoMaritalSt(String codCecivi);

}