package com.bbva.qwai.lib.r002.impl.enums;

public enum DocumentType {
	DNI("1"),CIF("2"),PASSPORT("3"),RESIDENTIAL_PASS("4"),NIF("5"), RUC("6"), CURP("7"), RUT("8"),INE("9"),DL("10");
	
	private String documentTypeCode;
	  
	private DocumentType(String documentType)
	{
	    this.documentTypeCode = documentType;
	}
	  
	public final String getDocumentCode()
	{
	    return this.documentTypeCode;
	}

}
