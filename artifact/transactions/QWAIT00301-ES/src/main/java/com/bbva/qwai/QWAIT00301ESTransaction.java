package com.bbva.qwai;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;

/**
 * GET by customerId Implementacion de logica de negocio.
 * 
 * @author javierdominguez
 *
 */
public class QWAIT00301ESTransaction extends AbstractQWAIT00301ESTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00301ESTransaction.class);

	@Override
	public void execute() {
		QWAIR001 qwaiR001 = (QWAIR001) getServiceLibrary(QWAIR001.class);

		if (QWAIT00301ESAction.GET.name().equals(getRestfulMethod())) {
			LOGGER.info("Estamos entrando al metodo GET/{customerId} del API Customers");

			List<String> paginationKeyList = getQueryParameter("paginationKey");
			String paginationKey = null;
			LOGGER.info("Obteniendo queryPram paginationKey");
			if (paginationKeyList != null && !paginationKeyList.isEmpty()) {
				LOGGER.info("Se encontró valor de queryParam paginationKey");
				paginationKey = paginationKeyList.get(0);
			} else {
				LOGGER.info("Se asigna valor default a queryParam paginationKey");
				paginationKey = "0";
			}

			List<String> pagSizeList = getQueryParameter("pageSize");
			String pagSize = null;
			if (pagSizeList != null && !pagSizeList.isEmpty()) {
				pagSize = pagSizeList.get(0);
			} else {
				pagSize = "10";
			}

			String customerId = getPathParameter("customerId");

			CustomerDTO retorno = qwaiR001.executeGet(customerId);

			if (this.getAdvice() != null) {
				if (this.getAdvice().getCode().equals("QWAI00841000")) {
					LOGGER.warn("No hubo registros");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_204, Severity.WARN);
				} else {
					// QWAI00841002
					LOGGER.error("Hubo un error al encontrar a los clientes");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
				}
			} else {
				LOGGER.debug("Busqueda exitosa");
				setEntity(retorno);
				setContentLocation(getURIPath());
				setHttpResponseCode(HttpResponseCode.HTTP_CODE_200, Severity.OK);
			}

			LOGGER.info("Hola somos el Grupo 5");
		}
	}
}
